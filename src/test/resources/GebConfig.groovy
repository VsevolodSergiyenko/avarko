/*
	This is the Geb configuration file.
	See: http://www.gebish.org/manual/current/configuration.html
*/

//Driver as the default, run configuration: “-ea”
driver = "firefox"

//Manage reports' directory and default screenshoting settings
reportsDir = "target/geb-reports"
reportOnTestFailureOnly = true

//manage waiting for Geb
waiting {
    timeout = 30
    retryInterval = 0.5
}

//Different browsers selection from config window
environments {

    // “-ea -Dgeb.env=firefox”
    firefox {
        driver = "firefox"
    }
    // “-ea -Dgeb.env=chrome”
    chrome {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/x86/chromedriver.exe")
        driver = "chrome"
    }

    // “-ea -Dgeb.env=ie32”
    ie32 {
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/x86/IEDriverServer.exe")
        driver = "ie"
    }

    // “-ea -Dgeb.env=ie64”
    ie64 {
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/x64/IEDriverServer.exe")
        driver = "ie"
    }

}