import Pages.FrontPage
import Pages.HotelsPage
import Pages.SearchResultsPage
import Pages.SummaryPage
import geb.spock.GebReportingSpec
import spock.lang.Stepwise;

@Stepwise
class AvarkoSpec extends GebReportingSpec {

    //Example Test Case description - PAGE 1: Frontpage www.aurinkomatkat.fi
    def "Sun trip front page test"() {

        //Go to www.aurinkomatkat.fi
        given: "Open http://aurinkomatkat.fi"
        to FrontPage

        when: "Was redirected to the front page"
        at FrontPage

        //Do following action:
        then: "Take screenshot"
        waitFor { departureCitySelectionDropDown }
        //Save page screenshot
        report("Front_page_screenshot")

        //Do following checks:
        when: "Text \"KOHTEET\" exists and \"Helsingistä\" is displayed"
        //Text “KOHTEET” exists
        assert selectionDropDownTitle == "KOHTEET"
        //Text “Helsingistä” loads to the search form
        assert selectedDepartureCity == "Helsingistä"

        //If checks pass, do action:
        then: "Click orange \"HAE\" button"
        //Click the orange box “HAE”
        searchButton.click()

    }

    //Example Test Case description - PAGE 2: Search results
    def "Search results page test"() {

        given: "Wait for the search results page"
        waitFor { SearchResultsPage }

        when: "Was redirected to the search results page"
        at SearchResultsPage

        //Do following action:
        then: "Take screenshot"
        waitFor { firstSearchResultSelectButton }
        // Save page screenshot
        report("Search results page screenshot")

        //Do following checks:
        when: "Text \"Haku\" exists"
        //Text “Haku” exists
        assert searchTabTitle == "Haku"
        waitFor { firstSearchResultSelectButton }

        //If checks pass, do action:
        then: "Click on the first select button"
        //Click first “VALITSE” link
        firstSearchResultSelectButton.click()

    }

    //Example Test Case description - PAGE 3: Hotels
    def "Hotels page test"() {

        given: "Wait for the hotels page"
        waitFor { HotelsPage }

        when: "Was redirected to the hotels page"
        at HotelsPage

        //Do following action:
        then: "Take screenshot"
        waitFor { firstHotelSelectButton }
        // Save page screenshot
        report("Hotels page screenshot")

        //Do following checks:
        when: "Text \"MAJOITUSVAIHTOEHDOT\" exists"
        //Text “MAJOITUSVAIHTOEHDOT” exists
        assert accomadationOptionsHeaderText.contains("MAJOITUSVAIHTOEHDOT")

        //If checks pass, do action:
        then: "Click on the first select(VALITSE) button"
        //Click first “VALITSE” link
        firstHotelSelectButton.click()

    }

    //Example Test Case description - PAGE 4: Summary
    def "Summary page test"() {

        given: "Wait for the summary page"
        waitFor { SummaryPage }

        when: "Was redirected to the summary page"
        at SummaryPage

        //Do following action:
        then: "Take screenshot"
        waitFor { priceColumnTableHeader }
        // Save page screenshot
        report("Summary page screenshot")

        //Do following checks:
        and: "Text \"Perushinta\" exists"
        //Text “Perushinta” exists
        assert priceColumnTitleText == "Perushinta"

    }
}
