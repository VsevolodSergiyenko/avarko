package Pages

import geb.Page

class HotelsPage extends Page {
    /**
     * "At checker" that the browser uses for checking if it is pointing at a given page
     */
    static at = { title.contains("Aurinkomatkat") }
    /**
     * General PageObject specification module
     */
    static content = {

        //PageObject geb controls
        /**
         * Accommodation options header
         */
        accomadationOptionsHeader { $(".span9>h1") }
        /**
         * First search results select button
         * parameter "to" specifies to which PageObject geb class WebDriver(geb "driver") instance should be passed
         */
        firstHotelSelectButton(to: SummaryPage) { $(".dynamicContinueLink", 0) }

        //Text data, gained from PageObject geb controls
        /**
         * Accommodation options header text
         */
        accomadationOptionsHeaderText { accomadationOptionsHeader.text().toUpperCase() }
    }

}
