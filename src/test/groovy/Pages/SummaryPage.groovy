package Pages

import geb.Page

class SummaryPage extends Page {

    /**
     * "At checker" that the browser uses for checking if it is pointing at a given page
     */
    static at = { title.contains("Aurinkomatkat") }
    /**
     * General PageObject specification module
     */
    static content = {

        //PageObject geb controls
        /**
         * Base price column header (in Summary table)
         */
        priceColumnTableHeader { $("#summary>thead th", 1) }

        //Text data, gained from PageObject geb controls
        /**
         * Base price column header text
         */
        priceColumnTitleText { priceColumnTableHeader.text() }

    }

}
