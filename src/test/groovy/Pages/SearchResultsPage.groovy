package Pages

import geb.Page

class SearchResultsPage extends Page {
    /**
     * "At checker" that the browser uses for checking if it is pointing at a given page
     */
    static at = { title.contains("Aurinkomatkat") }
    /**
     * General PageObject specification module
     */
    static content = {

        //PageObject geb controls
        /**
         * Travel booking screen search tab
         */
        searchTab { $("ol.step8>li>a") }
        /**
         * First search results select button
         * parameter "to" specifies to which PageObject geb class WebDriver(geb "driver") instance should be passed
         */
        firstSearchResultSelectButton(to: HotelsPage) { $(".price+.button[href]", 0) }

        //Text data, gained from PageObject geb controls
        /**
         * Search tab title
         */
        searchTabTitle { searchTab.text() }

    }

}
