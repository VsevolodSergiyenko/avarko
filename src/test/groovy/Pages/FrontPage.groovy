package Pages

import geb.Page

/**
 * Class for representing http://aurinkomatkat.fi front page
 */
class FrontPage extends Page {
    /**
     * Page URL definition used when using the driver "to()" method
     */
    static url = "http://aurinkomatkat.fi"
    /**
     * "At checker" that the browser uses for checking if it is pointing at a given page
     */
    static at = { title.contains("Lomamatkat") }
    /**
     * General PageObject specification module
     */
    static content = {

        //PageObject geb controls
        /**
         * Tab menu selection drop-down WebElement
         */
        selectionDropDown { $("nav>ul>li.sub>a[href]", 0) }
        /**
         * Departure city drop-down
         */
        departureCitySelectionDropDown { $("ol>li>input[value]", 0) }
        /**
         * Search button
         * parameter "to" specifies to which PageObject geb class WebDriver(geb "driver") instance should be passed
         */
        searchButton(to: SearchResultsPage) { $("fieldset.actions>button") }

        //Text data, gained from PageObject geb controls
        /**
         * Menu first tab title
         */
        selectionDropDownTitle { selectionDropDown.text() }
        /**
         * Selected departure city
         */
        selectedDepartureCity { departureCitySelectionDropDown.getAttribute("value") }

    }

}
